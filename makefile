# anewkirk

CC = cc
FLAGS = -Wall -Wextra -Wpedantic -Og -g -Wno-pointer-sign

# inline expansion optimizations DISABLED for compiling tests to allow
# for stringify-ing test conditions.
TFLAGS = $(FLAGS) -fno-inline

INCLUDES = -I.
LIBS = -lm

default:
	@make -s clean; make -s charstream; make -s lexer; make -s test; make -s repl

clean: $(shell find bin -type f)
	@rm -f ../bin/*.o;

charstream: $(shell find src -name character_stream*)
	@$(CC) $(FLAGS) -c src/charstream.c -o bin/charstream.o

lexer: $(shell find src -name lexer.*)
	@$(CC) $(FLAGS) -c -o bin/lexer.o src/lexer.c bin/charstream.o $(LIBS)

repl: src/repl.c
	@$(CC) $(FLAGS) -o bin/repl src/repl.c bin/lexer.o bin/charstream.o -lreadline -lm

test: $(shell find tests -name *.c)
	@$(CC) -o bin/test_cs tests/test_cs.c $(TFLAGS) $(INCLUDES)
	@$(CC) -o bin/test_lex tests/test_lexer.c bin/charstream.o $(LIBS) \
$(TFLAGS) 
	@bin/test_cs
	@bin/test_lex
