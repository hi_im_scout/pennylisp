/* anewkirk */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "minunit.h"
#include "../src/bool.h"
#include "../src/tokens.h"
#include "../src/charstream.h"

/* Entirety of source included to 
   facilitate testing of static functions
 */
#include "../src/lexer.c"

uint32_t tests_run = 0;
uint32_t asserts_run;

static Lexer *setup(uint8_t *code) {
  CharStream *cs = calloc(1, sizeof(CharStream));
  cstream_init(cs, code);
  Lexer *lex = calloc(1, sizeof(Lexer));
  init_lexer(lex, cs);
  return lex;
}

static uint8_t *test_next_token() {
  uint8_t *code = ";this is a comment\n(+      5   (* 2 30))";
  Lexer *lex = setup(code);
  Token *t = next_token(lex);
  mu_assert(t != NULL);
  mu_assert(t->token_type = TE_PAREN_TOKEN);
  mu_assert(t->value.paren = '(');

  t = next_token(lex);
  mu_assert(t->token_type == TE_OBJECT_TOKEN);
  mu_assert(t->value.obj->type == TE_SYM);
  mu_assert(!strcmp(t->value.obj->data.xsym, "+"));

  t = next_token(lex);
  mu_assert(t->token_type == TE_OBJECT_TOKEN);
  mu_assert(t->value.obj->type == TE_INTEGER);
  mu_assert(t->value.obj->data.xint == 5);

  t = next_token(lex);
  mu_assert(t->token_type == TE_PAREN_TOKEN);
  mu_assert(t->value.paren == '(');

  t = next_token(lex);
  mu_assert(t->token_type == TE_OBJECT_TOKEN);
  mu_assert(t->value.obj->type == TE_SYM);
  mu_assert(!strcmp(t->value.obj->data.xsym, "*"));

  t = next_token(lex);
  mu_assert(t->token_type == TE_OBJECT_TOKEN);
  mu_assert(t->value.obj->type == TE_INTEGER);
  mu_assert(t->value.obj->data.xint == 2);

  t = next_token(lex);
  mu_assert(t->token_type == TE_OBJECT_TOKEN);
  mu_assert(t->value.obj->type == TE_INTEGER);
  mu_assert(t->value.obj->data.xint == 30);

  t = next_token(lex);
  mu_assert(t->token_type == TE_PAREN_TOKEN);
  mu_assert(t->value.paren == ')');

    t = next_token(lex);
  mu_assert(t->token_type == TE_PAREN_TOKEN);
  mu_assert(t->value.paren == ')');
  return 0;
}

static uint8_t *test_parse_int() {
  uint8_t *t = "1234567";
  mu_assert(parse_int(t, false) == 1234567);
  
  t = "00002";
  mu_assert(parse_int(t, false) == 2);
  mu_assert(parse_int(t, true) == -2);

  t = "00000";
  mu_assert(parse_int(t, false) == 0);
  mu_assert(parse_int(t, true) == 0);

  t = "550055";
  mu_assert(parse_int(t, false) == 550055);
  
  return 0;
}

static uint8_t *test_parse_fractional_digits() {
  uint8_t *t = "1234562627";
  mu_assert(parse_fractional_digits(t) == 0.1234562627);
  
  t = "00002";
  mu_assert(parse_fractional_digits(t) == 0.00002);

  t = "00000";
  mu_assert(parse_fractional_digits(t) == 0);

  t = "550055";
  mu_assert(parse_fractional_digits(t) == 0.550055);
  
  return 0;
}

static uint8_t *test_unget_token() {
  uint8_t *code = "\0";
  Lexer *lex = setup(code);
  Token *t = calloc(1, sizeof(Token));
  t->token_type = TE_PAREN_TOKEN;
  t->value.paren = '(';
  unget_token(lex, t);
  mu_assert(next_token(lex) == t);
  return 0;
}

static uint8_t *test_is_digit() {
  mu_assert(is_digit('0'));
  mu_assert(is_digit('9'));
  mu_assert(!is_digit('a'));
  mu_assert(!is_digit(':'));
  mu_assert(!is_digit('/'));
  return 0;
}

static uint8_t *test_is_whitespace() {
  mu_assert(is_whitespace(' '));
  mu_assert(is_whitespace('\t'));
  mu_assert(is_whitespace('\n'));
  return 0;
}

static uint8_t *test_is_symbol_char() {
  mu_assert(is_symbol_char('a'));
  mu_assert(is_symbol_char('z'));
  mu_assert(!is_symbol_char('@'));
  mu_assert(!is_symbol_char('['));
  mu_assert(!is_symbol_char(' '));
  mu_assert(!is_symbol_char('{'));
  mu_assert(is_symbol_char('A'));
  mu_assert(is_symbol_char('Z'));
  mu_assert(is_symbol_char('+'));
  mu_assert(is_symbol_char('*'));
  return 0;
}

static uint8_t *test_is_comment_start() {
  mu_assert(is_comment_start(';'));
  return 0;
}

static uint8_t *test_is_string_delimiter() {
  mu_assert(is_string_delimiter('"'));
  return 0;
}

static uint8_t *test_read_while() {
  uint8_t *code = "(+ 12312 34534 asdf      +)";
  Lexer *lex = setup(code);
  next_token(lex);
  uint8_t *t = read_while(lex, is_symbol_char);

  mu_assert(!strcmp(t, "+"));
  
  cstream_eat_ws(lex->cs);
  t = read_while(lex, is_digit);
  mu_assert(!strcmp(t, "12312"));
  
  cstream_eat_ws(lex->cs);
  t = read_while(lex, is_digit);
  mu_assert(!strcmp(t, "34534"));
  
  cstream_eat_ws(lex->cs);
  t = read_while(lex, is_symbol_char);
  mu_assert(!strcmp(t, "asdf"));
  
  cstream_eat_ws(lex->cs);
  t = read_while(lex, is_symbol_char);
  mu_assert(!strcmp(t, "+"));
  return 0;
}

static uint8_t *test_read_number() {
  uint8_t *code = "    ;asdf\n0.3231 3.14159 1991";
  Lexer *lex = setup(code);

  cstream_eat_ws(lex->cs);
  cstream_eat_comment(lex->cs);

  Object *obj = read_number(lex);
  mu_assert(obj->type == TE_FLOAT);
  mu_assert(obj->data.xfloat == 0.3231);

  cstream_eat_ws(lex->cs);
  obj = read_number(lex);
  mu_assert(obj->type == TE_FLOAT);
  mu_assert(obj->data.xfloat == 3.14159);

  cstream_eat_ws(lex->cs);
  obj = read_number(lex);
  mu_assert(obj->type == TE_INTEGER);
  mu_assert(obj->data.xint == 1991);
  return 0;
}

static uint8_t *test_read_str() {
  uint8_t *code = ";comment\n     \"this is a string\" \"and another\" \"asdf\"" ;
  Lexer *lex = setup(code);
  uint8_t *strs[] = {"this is a string", "and another", "asdf"};
  Object *obj;

  for(uint8_t i = 0; i < 3; i++) {
    cstream_eat_comment(lex->cs);
    cstream_eat_ws(lex->cs);
    
    obj = read_str(lex);

    mu_assert(obj->type == TE_STRING);
    mu_assert(!strcmp(obj->data.xstr, strs[i]));
  }
  
  return 0;
}

static uint8_t *test_read_char() {
  uint8_t *code = "#\\r #\\a #\\t #\\s #\\ #\\a #\\b #\\c";
  Lexer *lex = setup(code);

  uint8_t *characters = "rats abc";
  Object *obj;

  for(uint32_t i = 0; i < strlen(characters); i++) {
    obj = read_char(lex);
    cstream_eat_ws(lex->cs);
    mu_assert(obj->type == TE_CHAR);
    mu_assert(obj->data.xchar == characters[i]);
  }
  
  return 0;
}

static uint8_t *test_read_bool() {
  uint8_t *code = "   ;casdf\n#t   #f  #f #t    ;comment\n#t";
  Lexer *lex = setup(code);

  cstream_eat_ws(lex->cs);
  cstream_eat_comment(lex->cs);
  Object *obj = read_bool(lex);
  mu_assert(obj->type == TE_BOOL);
  mu_assert(obj->data.xbool);

  cstream_eat_ws(lex->cs);
  obj = read_bool(lex);
  mu_assert(obj->type == TE_BOOL);
  mu_assert(!obj->data.xbool);

  cstream_eat_ws(lex->cs);
  obj = read_bool(lex);
  mu_assert(obj->type == TE_BOOL);
  mu_assert(!obj->data.xbool);

  cstream_eat_ws(lex->cs);
  obj = read_bool(lex);
  mu_assert(obj->type == TE_BOOL);
  mu_assert(obj->data.xbool);

  cstream_eat_ws(lex->cs);
  cstream_eat_comment(lex->cs);
  obj = read_bool(lex);
  mu_assert(obj->type == TE_BOOL);
  mu_assert(obj->data.xbool);

  return 0;
}

static uint8_t *test_read_sym() {
  uint8_t *code = "symbol a b c penny faye";
  Lexer *lex = setup(code);

  const uint8_t *syms[] = {"symbol", "a", "b", "c", "penny", "faye"};
  Object *obj;
  for(uint8_t i = 0; i < 6; i++) {
    cstream_eat_ws(lex->cs);
    obj=read_sym(lex);
    mu_assert(obj->type == TE_SYM);
    mu_assert(!strcmp(obj->data.xsym, syms[i]));
  }
  
  return 0;
}

static uint8_t *all_tests() {
  mu_run_test(test_unget_token);
  mu_run_test(test_is_digit);
  mu_run_test(test_is_whitespace);
  mu_run_test(test_is_symbol_char);
  mu_run_test(test_is_comment_start);
  mu_run_test(test_is_string_delimiter);
  mu_run_test(test_is_digit);
  mu_run_test(test_read_while);
  mu_run_test(test_parse_int);
  mu_run_test(test_parse_fractional_digits);
  mu_run_test(test_next_token);
  mu_run_test(test_read_number);
  mu_run_test(test_read_str);
  mu_run_test(test_read_char);
  mu_run_test(test_read_bool);
  mu_run_test(test_read_sym);
  return 0;
}

int main(void) {
  mu_run_suite("Lexical Analyzer");
}
