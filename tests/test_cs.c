/* anewkirk */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "minunit.h"
#include "../src/bool.h"
#include "../src/charstream.c"

uint32_t tests_run = 0;
uint32_t asserts_run = 0;

static CharStream *setup(uint8_t *code) {
  CharStream *cs = calloc(1, sizeof(CharStream));
  cstream_init(cs, code);
  return cs;
}

static uint8_t *test_next() {
  uint8_t *code = "(car '(1 2 3))";
  CharStream *cs = setup(code);
  for(uint32_t i = 0; i < strlen(code); i++) {
    mu_assert(cstream_next(cs) == code[i]);
  }
  return 0;
}

static uint8_t *test_peek() {
  uint8_t *code = "(car '(1 2 3))";
  CharStream *cs = setup(code);
  for(uint32_t i = 0; i < strlen(code); i++) {
    mu_assert(cstream_peek(cs) == code[i]);
    cstream_next(cs);
  }
  return 0;
}

static uint8_t *test_peekx() {
  uint8_t *code = "(car '(1 2 3))";
  CharStream *cs = setup(code);
  for(uint32_t i = 0; i < strlen(code); i++) {
    mu_assert(cstream_peekx(cs, i) == code[i]);
  }
  return 0;
}

static uint8_t *test_eof() {
  uint8_t *code = "(car '(1 2 3))";
  CharStream *cs = setup(code);
  for(uint32_t i = 0; i < strlen(code); i++) {
    cstream_next(cs);
  }
  mu_assert(cstream_eof(cs));
  return 0;
}

static uint8_t *test_eat_ws() {
  uint8_t *code = "       (cdr    '(12 3 4))   ()";
  CharStream *cs = setup(code);
  cstream_eat_ws(cs);

  mu_assert(cstream_peek(cs) == '(');

  for(uint8_t i = 0; i < 4; i++) {
    cstream_next(cs);
  }
  
  cstream_eat_ws(cs);
  mu_assert(cstream_peek(cs) == '\'');
  return 0;
}

static uint8_t *test_eat_comment() {
  uint8_t *code = ";this is a comment\n(car '(1 2 3))";
  CharStream *cs = setup(code);
  cstream_eat_comment(cs);
  mu_assert(cstream_peek(cs) == '(');
  return 0;
}

static uint8_t *all_tests() {
  mu_run_test(test_next);
  mu_run_test(test_peek);
  mu_run_test(test_peekx);
  mu_run_test(test_eof);
  mu_run_test(test_eat_ws);
  mu_run_test(test_eat_comment);
  return 0;
}

int main(void) {
  mu_run_suite("Character Stream");
}
