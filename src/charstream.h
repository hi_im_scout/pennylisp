/* anewkirk */

#pragma once
#include <stdint.h>
#include "bool.h"

typedef struct CharStream {
  uint8_t *start;
  uint8_t *current;
  uint64_t line;
  uint64_t col;
} CharStream;

/* Initializes a Character Stream with default values and sets the
   'current' pointer to the beginning of the source code */
void cstream_init(CharStream *cs, uint8_t *source);

/* Advances the 'current' pointer by one character, and returns the 
   character that was at the pointer previously. */
uint8_t cstream_next(CharStream *cs);

/* Returns the character at the 'current' pointer. */
uint8_t cstream_peek(CharStream *cs);

/* Returns the character at current[x] */
uint8_t cstream_peekx(CharStream *cs, uint16_t x);

/* Returns true if the character at the 'current' pointer is equal
   to NUL ('\0') */
bool cstream_eof(CharStream *cs);

/* Advances the 'current' pointer until the character located at it 
is no longer a whitespace character (space, newline, tab). */
void cstream_eat_ws(CharStream *cs);

/* Advances until after a newline is reached. */
void cstream_eat_comment(CharStream *cs);
