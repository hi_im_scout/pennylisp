/* anewkirk */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <readline/readline.h>
#include <readline/history.h>

#include "lexer.h"
#include "charstream.h"
#include "model.h"
#include "parser.h"
#include "evaluator.h"
#include "gc.h"

#define PROMPT "[e.e]>> "
#define BUFLEN 10000

int main(int argc, char **argv) {
  puts("PennyLisp 0.1.1");
  puts("Press Ctrl+C to exit\n");

  uint8_t *input = NULL;

  while(1) {
    input = readline(PROMPT);
    add_history(input);
    puts(input);
    free(input);
  }
  return 0;
}
