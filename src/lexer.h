/* anewkirk */

#pragma once
#include <stdint.h>
#include "charstream.h"
#include "model.h"
#include "tokens.h"
#include "bool.h"

typedef struct _lexer {
  CharStream* cs;
  Token *previous;
} Lexer;

Token *next_token(Lexer *lex);

void init_lexer(Lexer* lex, CharStream *cs);

void unget_token(Lexer *lex, Token *t);
