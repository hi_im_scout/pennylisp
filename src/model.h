/* anewkirk */

#pragma once
#include <stdint.h>
#include "bool.h"

typedef struct _object Object;
typedef struct _pair Pair;

typedef enum _object_type {
  TE_FLOAT,
  TE_INTEGER,
  TE_STRING,
  TE_CHAR,
  TE_BOOL,
  TE_SYM,
  TE_PROC,
  TE_LAMBDA,
  TE_PAIR
} ObjectType;

typedef struct _pair {
  Object *carObject;
  Pair *cdrObject;
} Pair;

typedef struct _environment {
  struct _environment *enclosing_scope;
  Pair vars;
  Pair vals;
} Environment;

typedef struct _lambda {
  Environment *env;
  Pair *fn;
} Lambda;

typedef struct _object {
  ObjectType type;
  bool gcmark;
  union ObjectData {
    double xfloat;
    int64_t xint;
    uint8_t xchar;
    uint8_t *xstr;
    bool xbool;
    uint8_t *xsym;
    Pair xpair;
    Object *(*xprproc)(Pair*);
    Lambda *xlambda;
  } data;
} Object;
