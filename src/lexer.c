/* anewkirk */

#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "bool.h"
#include "lexer.h"
#include "charstream.h"

#define INITIAL_TOKEN_LEN 32

static Object *new_object();
static int64_t parse_int(uint8_t *str, bool neg);
static double parse_fractional_digits(uint8_t *str);
static Object *read_number(Lexer *lex);
static Object *read_str(Lexer *lex);
static Object *read_char(Lexer *lex);
static Object *read_bool(Lexer *lex);
static Object *read_sym(Lexer *lex);
static uint8_t *read_while(Lexer *lex, bool (*predicate)(uint8_t));
static bool is_digit(uint8_t c);
static bool is_whitespace(uint8_t c);
static bool is_symbol_char(uint8_t c);
static bool is_comment_start(uint8_t c);
static bool is_string_delimiter(uint8_t c);
static bool is_delimiter(uint8_t c);


static Object *new_object() {
  Object *obj = calloc(1, sizeof(Object));
  obj->gcmark = false;
  return obj;
}

static int64_t parse_int(uint8_t *str, bool neg) {
  int8_t sign = neg? -1 : 1;
  int64_t num = 0;

  for(uint32_t i = 0; i < strlen(str); i++) {
    num = (num * 10) + (str[i] - '0');
  }

  num *= sign;
  return num;
}

static double parse_fractional_digits(uint8_t *str) {
  uint32_t digits_len = strlen(str);
  int64_t num = parse_int(str, false);
  double divisor = pow(10, digits_len);
  return num/divisor;
}

static Object *read_number(Lexer *lex) {
  bool neg = false;
  if(cstream_peek(lex->cs) == '-') {
    neg = true;
    cstream_next(lex->cs);
  }
  
  uint8_t *integer_digits = read_while(lex, is_digit);
  int64_t integer_part = parse_int(integer_digits, neg);
  
  uint8_t c = cstream_peek(lex->cs);
  Object *obj = new_object();
  
  if(c == '.') {
    cstream_next(lex->cs);
    uint8_t *fractional_digits = read_while(lex, is_digit);
    double fractional_part = 0;
    if(strlen(fractional_digits)) {
      fractional_part = parse_fractional_digits(fractional_digits);
    } 
    double result = integer_part;
    if(result < 0) {
      result -= fractional_part;
    } else {
      result += fractional_part;
    }
    obj->type = TE_FLOAT;
    obj->data.xfloat = result;
  } else {
    obj->type = TE_INTEGER;
    obj->data.xint = integer_part;
  }
  return obj;
}

static Object *read_char(Lexer *lex) {
  // Skip hash ('#')
  cstream_next(lex->cs);

  // Skip leading backslash
  cstream_next(lex->cs);
  
  uint8_t c = cstream_next(lex->cs);
  Object *obj = new_object();
  if(c == '\\') {
    uint8_t escaped = cstream_next(lex->cs);
    if(escaped == 'n') {
      c = '\n';
    } else if(escaped == 't') {
      c = '\t';
    } else {
      free(obj);
      return NULL;
    }
  }
  obj->type = TE_CHAR;
  obj->data.xchar = c;
  return obj;
}

static Object *read_bool(Lexer *lex) {
  // Skip hash ("#")
  cstream_next(lex->cs);
  
  uint8_t c = cstream_next(lex->cs);
  Object *obj = new_object();
  obj->type = TE_BOOL;
  if(c == 't') {
    obj->data.xbool = true;
  } else if(c == 'f') {
    obj->data.xbool = false;
  } else {
    free(obj);
    return NULL;
  }
  return obj;
}

static Object *read_str(Lexer *lex) {
  uint32_t max_len = INITIAL_TOKEN_LEN;
  uint8_t *str = realloc(NULL, max_len);
  uint32_t len = 0;

  //skip opening quotes
  cstream_next(lex->cs);
  
  uint8_t c = cstream_next(lex->cs);
  while(!cstream_eof(lex->cs)) {
    if(c == '"') {
      break;
    }
    
    else if(c == '\\') {
      uint8_t p = cstream_peek(lex->cs);
      if(p == '"') {
	c = '"';
      } else if(p == 'n') {
	c = '\n';
      } else if(p == 't') {
	c = '\t';
      } else {
	//FIXME throw error
      }
      cstream_next(lex->cs);
    }

    str[len++] = c;
    if(len >= max_len - 1) {
      max_len *= 2;
      str = realloc(str, max_len);
    }
    c = cstream_next(lex->cs);
  }
  str[len++] = '\0';
  str = realloc(str, len);
  Object *obj = new_object();
  obj->type = TE_STRING;
  obj->data.xstr = str;
  return obj;
}

static Object *read_sym(Lexer *lex) {
  uint8_t *s = read_while(lex, is_symbol_char);
  Object *obj = new_object();
  obj->type = TE_SYM;
  obj->data.xsym = s;
  return obj;
}

static uint8_t *read_while(Lexer *lex, bool (*predicate)(uint8_t)) {
  uint32_t len = 0;
  uint32_t max_length = INITIAL_TOKEN_LEN;
  uint8_t *str = realloc(NULL, max_length);
  str[0] = '\0';
  while(!cstream_eof(lex->cs) &&
	predicate(cstream_peek(lex->cs))) {
    
    str[len++] = cstream_next(lex->cs);
    if(len >= max_length - 1) {
      max_length *= 2;
      str = realloc(str, max_length);
    }
    
  }
  str[len++] = '\0';
  return realloc(str, len);
}

static bool is_digit(uint8_t c) {
  return (c >= 48 && c <= 57);
}

static bool is_whitespace(uint8_t c) {
  return (c == ' ' || c == '\n' || c == '\t');
}

static bool is_symbol_char(uint8_t c) {
  bool result = false;

  //check for uppercase letters
  result += (c >= 65 && c <= 90);

  //check for lowercase letters
  result += (c >= 97 && c <= 122);

  //check against a few other character literals...
  result += (c == '+');
  result += (c == '-');
  result += (c == '*');
  result += (c == '/');

  return result;
}

static bool is_comment_start(uint8_t c) {
  return c == ';';
}

static bool is_delimiter(uint8_t c) {
  return c == ' ' || c == '(' || c == ')';
}

static bool is_string_delimiter(uint8_t c) {
  return c == '"';
}

void init_lexer(Lexer *lex, CharStream *cs) {
  lex->cs = cs;
  lex->previous = NULL;
}

void unget_token(Lexer *lex, Token *t) {
  lex->previous = t;
}

Token *next_token(Lexer *lex) {
  if(lex->previous != NULL) {
    Token *t = lex->previous;
    lex->previous = NULL;
    return t;
  }

  if(cstream_eof(lex->cs)) {
    return NULL;
  }

  cstream_eat_ws(lex->cs);

  Token *r = calloc(1, sizeof(Token));
  r->token_type = TE_OBJECT_TOKEN;
  uint8_t c = cstream_peek(lex->cs);

  if(is_comment_start(c)) {
    cstream_eat_comment(lex->cs);
    cstream_eat_ws(lex->cs);
    return next_token(lex);
  }

  else if(c == '(' || c == ')') {
    cstream_next(lex->cs);
    r->token_type = TE_PAREN_TOKEN;
    r->value.paren = c;
  }

  else if(c == '#') {
    c = cstream_peekx(lex->cs, 2);
    if(c == '\\') {
      r->value.obj = read_char(lex);      
    } else if(c == 't' || c == 'f') {
      r->value.obj = read_bool(lex);
    } else {
      free(r);
      return NULL;
    }
  }

  else if(is_digit(c) ||
	  (c == '-' && is_digit(cstream_peekx(lex->cs, 2)))) {
    r->value.obj = read_number(lex);
  }

  else if(is_string_delimiter(c)) {
    r->value.obj = read_str(lex);
  }

  else if(is_symbol_char(c)) {
    r->value.obj = read_sym(lex);
  }

  else {
    return NULL;
  }
  
  return r;
}
