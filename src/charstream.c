/* anewkirk */

#include "charstream.h"

void cstream_init(CharStream *cs, uint8_t *source) {
  cs->start = source;
  cs->current = source;
  cs->line = 0;
  cs->col = 0;
}

uint8_t cstream_next(CharStream *cs) {
  uint8_t current = cs->current[0];

  /* don't advance if the end of the string
     has been reached. */
  if(current != '\0') {
    cs->current += 1;
  }
  
  if(current == '\n') {
    cs->line++;
    cs->col = 0;
  } else {
    cs->col++;
  }

  return current;
}

uint8_t cstream_peekx(CharStream *cs, uint16_t x) {
  uint8_t c = '\0';
  for(uint16_t i = 0; i <= x; i++) {
    c = cs->current[i];
    if(c == '\0') {
      break;
    }
  }    
  return c;
}

uint8_t cstream_peek(CharStream *cs) {
  return cs->current[0];
}

bool cstream_eof(CharStream *cs) {
  return cs->current[0] == '\0';
}

void cstream_eat_ws(CharStream *cs) {
  while(!cstream_eof(cs)) {
    uint8_t current = cstream_peek(cs);
    if(!(current == ' ' || current == '\n' || current == '\t')) {
      break;
    }
    cstream_next(cs);
  }
}

void cstream_eat_comment(CharStream *cs) {
  if(cstream_peek(cs) == ';') {
    while(!cstream_eof(cs) && cstream_peek(cs) != '\n') {
      cstream_next(cs);
    }
    if(!cstream_eof(cs)) {
      cstream_next(cs);
    }
  }
}


