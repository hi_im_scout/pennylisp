/* anewkirk */

#pragma once
#include <stdint.h>
#include "model.h"

typedef enum _token_type {
  TE_OBJECT_TOKEN,
  TE_PAREN_TOKEN
} TokenType;

typedef struct _token {
  TokenType token_type;
  union TokenValue {
    uint8_t paren;
    Object *obj;
  } value;
} Token;
