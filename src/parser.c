/* anewkirk */

#include "parser.h"
#include "tokens.h"
#include "lexer.h"

static Object *read_pair(Lexer *lex);

Object *read(Lexer *lex) {
  Token *t = next_token(lex);
  if(t->token_type == TE_PAREN_TOKEN &&
     t->value.paren == '(') {
    return read_pair(lex);
  } else {
    return t->value.obj;
  }
}

static Object *read_pair(Lexer *lex) {
  Token *t = next_token(lex);
  if(t->token_type == TE_PAREN_TOKEN &&
     t->value.paren == ')') {
    // Return nil (the empty list)
  }
  unget_token(lex, t);
  Object *car = read(lex);
  Object *cdr = read_pair(lex);
  // return cons(car, cdr)
}
